package healthcare.alliedworld.dispergo.redeempoint.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import healthcare.alliedworld.dispergo.redeempoint.entity.PointsRedeemHistory
import healthcare.alliedworld.dispergo.redeempoint.repo.PointsRedeemHistoryeRepo
import io.reactivex.Completable
import io.reactivex.Single
import reach52.marketplace.community.resident.entity.Resident
import reach52.marketplace.community.resident.repo.ResidentRepo


class RedeemPointViewModel : ViewModel() {



    //lateinit var repository: Repository
    //lateinit var preferences: SecurePreferences
   // lateinit var userDatails: LoginResponseModel.Data
    var residentId = ""
    var resident = Resident()
    var isLogisticResident = true

    val pointsRedeemHistoryObserv = MutableLiveData<ArrayList<PointsRedeemHistory>>()
    var updatedata=""
    var insertdata=""

    init{
       // repository = repo
       // preferences = SecurePreferences(Reach52MasterApp.getAppContext()!!, Constant.preferenceName, Constant.secureKey, true)
        val gson = Gson()
       // val json: String = preferences.getString(Constant.USER_DETAILS)!!
       // userDatails = gson.fromJson(json, LoginResponseModel.Data::class.java)

      /*  val userDatails: LoginResponseModel.Data =
            gson.fromJson(agent, LoginResponseModel.Data::class.java)*/
    }


    fun loadResidentForId(context: Context, id: String) = Single.create<Resident> {
        residentId = id
        ResidentRepo.fetchResident(context, id).subscribe({ resi ->

            resident = resi
            it.onSuccess(resident)

        }, {
            it.printStackTrace()
        })

    }




    fun loadpointsRedeemHistory(context: Context, id: String) {

        PointsRedeemHistoryeRepo.getAllPointsRedeemHistory(context, id).subscribe({
            pointsRedeemHistoryObserv.value?.clear()
            Log.d("rtaaag", "hello: $it")
            pointsRedeemHistoryObserv.value = it

        }, {
            Log.d("rtaaaag", it.toString())
        })

    }
   /* lateinit var _getRedeemPointData: MutableLiveData<List<PointsRedeemHistory>>
    val getRedeemPointData: LiveData<List<PointsRedeemHistory>>
        get() = _getRedeemPointData

    fun getRedeemPoint(id:String) {
        _getRedeemPointData = MutableLiveData()
        _getRedeemPointData.value = repository.getRedeemPoint(id)
        println("_getRedeemPointData" + _getRedeemPointData.value!!.size)
    }




    lateinit var _savepointRedeemPoint: MutableLiveData<String>
    val savepointRedeemPoint: LiveData<String>
        get() = _savepointRedeemPoint

    fun savepointRedeemPoint(item: PointsRedeemHistory) {
        _savepointRedeemPoint = MutableLiveData()
        _savepointRedeemPoint.value = repository.savepointRedeemPoint(item)
    }



    // get resident detail
    lateinit var _residentDetailsLiveData: MutableLiveData<ResidentUser>
    val residentDetailsLiveData: LiveData<ResidentUser>
        get() = _residentDetailsLiveData

    fun getResidentDetails(id:String) {
        _residentDetailsLiveData = MutableLiveData<ResidentUser>()
        _residentDetailsLiveData.value = repository.getResidentDetails(id)

        //realmResident = _residentDetailsLiveData.value!!
    }

    fun updateRedeempoint(id: String, total: Double) {
        repository.updateRedeempoint(id,total)
    }*/


    fun savepointRedeemPoint(context: Context, item: PointsRedeemHistory) = Completable.create {

        PointsRedeemHistoryeRepo.savepointRedeemPoint(context, item).subscribe({
            Log.d("done1", "savepointRedeemPoint: " + "Done")
            insertdata = "done"
            it.onComplete()

        }, { err ->
            insertdata = "error"
            Log.d("error1", "savepointRedeemPoint: " + "Done")
            it.onError(err)
        }

        )
    }

    fun updateredeempointDb(context: Context, id: String, rt: Double, resident: Resident) = Completable.create {
        ResidentRepo.updateredeemPoint(context, id, rt, resident).subscribe({
            Log.d("done1", "updateredeempointDb: "+ "Done")
            updatedata="done"
            it.onComplete()

        }, { err ->
            updatedata="error"
            Log.d("error1", "updateredeempointDb: "+ "Done")
            it.onError(err)
        }

        )
    }

}