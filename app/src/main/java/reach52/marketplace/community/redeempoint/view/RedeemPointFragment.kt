package healthcare.alliedworld.dispergo.redeempoint.view

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import healthcare.alliedworld.dispergo.redeempoint.adapter.RedeempointAdapter
import healthcare.alliedworld.dispergo.redeempoint.entity.PointsRedeemHistory
import healthcare.alliedworld.dispergo.redeempoint.viewmodel.RedeemPointViewModel
import org.threeten.bp.ZoneOffset
import org.threeten.bp.ZonedDateTime
import reach52.marketplace.community.R
import reach52.marketplace.community.activities.BaseActivity
import reach52.marketplace.community.databinding.FragmentRedeempointBinding
import reach52.marketplace.community.databinding.RedeemAwardSelectBinding
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt


class RedeemPointFragment : Fragment() {


    // var pointsRedeemList: ArrayList<PointsRedeemHistory> = ArrayList<PointsRedeemHistory>()
    lateinit var viewModel: RedeemPointViewModel
    lateinit var binding: FragmentRedeempointBinding
    // lateinit var adapter: RedeempointAdapter
    lateinit var selected_radio: RadioButton
    private lateinit var listObserver: Observer<ArrayList<PointsRedeemHistory>>
    var pointsRedeemList: ArrayList<PointsRedeemHistory> = ArrayList<PointsRedeemHistory>()
    lateinit var adapter: RedeempointAdapter


    var selectedId: Int = 0

    @SuppressLint("ClickableViewAccessibility", "CheckResult")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_redeempoint, container, false)
        viewModel = ViewModelProvider(activity as BaseActivity)[RedeemPointViewModel::class.java]


        viewModel.loadResidentForId(requireContext(), viewModel.residentId).subscribe(
            {
                Log.d("RRRRdone", "message = $it")

                if (it.totalPoints.toString() == "null") {
                    binding.proftext2.text = "0"
                } else {
                    binding.proftext2.text = it.totalPoints.toString()
                    Log.d("list 2", "getRedeemPoint: " + it.totalPoints.toString())
                }
            },
            {
                Log.d("error", "message = $it")
            }
        )


        binding.redeemlist.layoutManager = LinearLayoutManager(requireContext())
        binding.redeemlist.setHasFixedSize(true)
        viewModel.loadpointsRedeemHistory(context!!, viewModel.residentId)
        viewModel.pointsRedeemHistoryObserv.observe(requireActivity(), {

            Log.d("list", "getRedeemPoint: " + it.size)
            val finallist: ArrayList<PointsRedeemHistory>
            if (!it.isEmpty()) {
                it.forEach {
                    pointsRedeemList.add(it)
                }
                if (pointsRedeemList.size <= 4) {
                    finallist = pointsRedeemList
                } else {
                    finallist = getlist(pointsRedeemList, 4) as ArrayList<PointsRedeemHistory>
                }
                adapter =
                    RedeempointAdapter(
                        requireActivity(),
                        finallist as List<PointsRedeemHistory>
                    )
                binding.redeemlist.adapter = adapter
                adapter.notifyDataSetChanged()

            }
        })


        binding.viwealltxt2.setOnClickListener {

            viewModel.loadpointsRedeemHistory(context!!, viewModel.residentId)
            viewModel.pointsRedeemHistoryObserv.observe(requireActivity(), {
                adapter = RedeempointAdapter(requireActivity(), it as List<PointsRedeemHistory>)
                binding.redeemlist.adapter = adapter
                adapter.notifyDataSetChanged()
            })


        }


        binding.btnredeem.setOnClickListener {

            var selected_itemValue = 0.0
            var selected_radio_value = ""
            val dialogBuilder: androidx.appcompat.app.AlertDialog.Builder =
                androidx.appcompat.app.AlertDialog.Builder(
                    requireContext()
                )
            val bindingConfirm: RedeemAwardSelectBinding = DataBindingUtil.inflate(
                LayoutInflater.from(context), R.layout.redeem_award_select, null, false)
            dialogBuilder.setView(bindingConfirm.root)
            val alertDialog: androidx.appcompat.app.AlertDialog = dialogBuilder.create()
            alertDialog.setCancelable(false)
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.show()
            bindingConfirm.redeemPlace.isEnabled = false

            bindingConfirm.cancel2.setOnClickListener {
                alertDialog.dismiss()
                // deleteButton.isEnabled = true
            }

            bindingConfirm.awardrdgrp2.setOnCheckedChangeListener { group, checkedId ->

                selectedId = bindingConfirm.awardrdgrp2.checkedRadioButtonId
                selected_radio = group.findViewById(selectedId)
                bindingConfirm.redeemPlace.isEnabled = true
                Log.d(
                    "idd 22",
                    "onCreateView: " + selected_radio.text + "  " + selectedId.toString()
                )

            }
            bindingConfirm.redeemPlace.setOnClickListener {

                Log.d(
                    "idd 2",
                    "onCreateView: " + selected_radio.text + "  " + selectedId.toString()
                )


                if (selected_radio.text.equals("Small Award (5 pts)")) {
                    selected_radio_value = "Small Award"
                    selected_itemValue = 5.00

                } else if (selected_radio.text.equals("Medium Award (50 pts)")) {
                    selected_radio_value = "Medium Award"
                    selected_itemValue = 50.00

                } else if (selected_radio.text.equals("Large Award (100 pts)")) {
                    selected_radio_value = "Large Award"
                    selected_itemValue = 100.00

                } else if (selected_radio.text.equals("Big Winner (500 pts)")) {
                    selected_radio_value = "Big Winner"
                    selected_itemValue = 500.00

                } else if (selected_radio.text.equals("reach52 Rockstar (1,000 pts)")) {
                    selected_radio_value = "reach52 Rockstar"
                    selected_itemValue = 100.00

                }

                if (((binding.proftext2.text as String).toDouble()) >= selected_itemValue) {
                    val item = PointsRedeemHistory(
                        ZonedDateTime.now(ZoneOffset.UTC),
                        selectedId,
                        selected_radio_value,
                        selected_itemValue,
                        viewModel.residentId  )
                    bindingConfirm.redeemPlace.isEnabled = false

                    viewModel.savepointRedeemPoint(requireContext(), item).subscribe {
                        //viewModel.savepointRedeemPoint(item)
                        // viewModel.savepointRedeemPoint(requireContext(), item).observe(viewLifecycleOwner, {
                        Log.d("result", "onCreateView: " + it)
                        if (viewModel.insertdata.equals("done")) {
                            val total: Int = (binding.proftext2.text.toString().toDouble() - item.itemValue.toDouble()).roundToInt()
                            viewModel.updateredeempointDb(requireContext(), viewModel.resident.id, total.toDouble(), viewModel.resident).subscribe {
                                viewModel.loadResidentForId(requireContext(), viewModel.residentId).subscribe(
                                    {
                                        Log.d("done", "message = $it")

                                        if (it.totalPoints.toString() == "null") {
                                            binding.proftext2.text = "0"
                                        } else {
                                            binding.proftext2.text = it.totalPoints.toString()
                                            Log.d(
                                                "list 2",
                                                "getRedeemPoint: " + it.totalPoints.toString()
                                            )
                                        }

                                        viewModel.loadpointsRedeemHistory(context!!, viewModel.residentId)
                                        viewModel.pointsRedeemHistoryObserv.observe(requireActivity(), {
                                            adapter = RedeempointAdapter(requireActivity(), it as List<PointsRedeemHistory>)
                                            binding.redeemlist.adapter = adapter
                                            adapter.notifyDataSetChanged()
                                        })

                                    },
                                    {
                                        Log.d("error", "message = $it")
                                    }
                                )

                                // binding.proftext2.text = total.toString()
                                Toast.makeText(
                                    requireContext(),
                                    getString(R.string.record_saved_successfully),
                                    Toast.LENGTH_SHORT
                                ).show()
                                alertDialog.dismiss()
                                adapter.notifyDataSetChanged()
                                //deleteButton.isEnabled = true

                            }
                        } else {
                            Toast.makeText(requireContext(), "not insert", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                } else {
                    Toast.makeText(
                        requireContext(), "Can't Redeem Point",
                        //getString(R.string.record_saved_successfully),
                        Toast.LENGTH_SHORT
                    ).show()
                    alertDialog.dismiss()


                    //deleteButton.isEnabled = true
                }
            }

        }


        return binding.root

    }




    fun getlist(it: List<PointsRedeemHistory>, ii: Int): Any {
        val pointsRedeemList2= ArrayList<PointsRedeemHistory>()
        for(i in 0..ii)
        {
            pointsRedeemList2.add(it.get(i))
        }
        Log.d("TAG", "getlist: "+pointsRedeemList2.size)

        return pointsRedeemList2
    }
    /* fun getRedeemPoint(residentKeyId: String) {
        binding.redeemlist.layoutManager = LinearLayoutManager(requireContext())
        binding.redeemlist.setHasFixedSize(true)

        viewModel.getRedeemPoint(residentKeyId)
        viewModel.getRedeemPointData.observe(viewLifecycleOwner, {
            Log.d("list", "getRedeemPoint: " + it.size)
            adapter = RedeempointAdapter(
                requireActivity(), it as List<PointsRedeemHistory>
            )
            binding.redeemlist.adapter = adapter
        })

    }*/

}