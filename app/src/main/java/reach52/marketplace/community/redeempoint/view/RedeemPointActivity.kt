package healthcare.alliedworld.dispergo.redeempoint.view

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import healthcare.alliedworld.dispergo.redeempoint.viewmodel.RedeemPointViewModel
import kotlinx.android.synthetic.main.activity_insurance_purchase.*
import reach52.marketplace.community.R
import reach52.marketplace.community.activities.BaseActivity
import reach52.marketplace.community.insurance.view.InsurancePurchaseActivity
import reach52.marketplace.community.resident.view.NewResidentActivity
import reach52.marketplace.community.resident.view.ResidentDetailsActivity


class RedeemPointActivity: BaseActivity() {
    companion object {
        val KEY_UPDATE = "update"
        const val KEY_RESIDENT_ID = "res_id"
    }

    private lateinit var vm: RedeemPointViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_redeempoint)
        setSupportActionBar(toolbar)

       // val residentId = intent.getStringExtra(VaccinationHistoryListActivity.KEY_RESIDENT_ID)!!
        val residentId = intent.getStringExtra(InsurancePurchaseActivity.KEY_RESIDENT_ID)
        vm = ViewModelProvider(this)[RedeemPointViewModel::class.java]
        vm.residentId = intent.getStringExtra(NewResidentActivity.KEY_RESIDENT_ID)
        vm.isLogisticResident = intent.getBooleanExtra(ResidentDetailsActivity.KEY_IS_LOGISTIC, true)
        //vm.init(ServiceLocator.provideRepository(this))

        //viewModel.residentId = intent.getStringExtra(Constant.INTENT_KEY_RESIDENT_ID)!!
        gotoFragment(redeempointFragment, true)

    }



    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        super.onPrepareOptionsMenu(menu)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onResume() {
        super.onResume()

    }

    private val redeempointFragment by lazy { RedeemPointFragment() }

    fun goToRedeempointFragment() {
        gotoFragment(redeempointFragment, emptyStack = false, addToBackStack = true)

    }
}