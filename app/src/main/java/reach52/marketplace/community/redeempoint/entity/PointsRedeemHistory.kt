package healthcare.alliedworld.dispergo.redeempoint.entity

import org.threeten.bp.ZonedDateTime
import java.util.*

class PointsRedeemHistory(
    var redeemDate: ZonedDateTime,
    var itemId : Int,
    var itemName : String,
    var itemValue: Double,
    var residentId: String
)