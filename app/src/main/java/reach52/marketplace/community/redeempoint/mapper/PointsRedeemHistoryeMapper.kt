package healthcare.alliedworld.dispergo.redeempoint.mapper

import healthcare.alliedworld.dispergo.redeempoint.entity.PointsRedeemHistory
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter
import reach52.marketplace.community.extensions.utils.zonedDateTimeToString
import reach52.marketplace.community.persistence.Unmarshaler

class PointsRedeemHistoryeMapper: Unmarshaler<PointsRedeemHistory> {


    private val KEY_REDEEMDATE= "redeemDate"
    private val KEY_ITEMID= "itemId"
    private val KEY_ITEMNAME= "itemName"
    private val KEY_ITEMVALUE= "itemValue"
    private val KEY_RESIDENTID= "residentId"


    fun marshal(purchase: PointsRedeemHistory): Map<String, Any> {
        val product = purchase
        val map = HashMap<String, Any>()
        map["type"] = "PointsRedeemHistory"
        map["redeemDate"] = zonedDateTimeToString(purchase.redeemDate)
        map["itemId"] = purchase.itemId
        map["itemName"] = purchase.itemName
        map["itemValue"] = purchase.itemValue
        map["residentId"] = purchase.residentId
        return  map

    }

    override fun unmarshal(properties: Map<String, Any>): PointsRedeemHistory {

        //val redeemDate = properties["redeemDate"] as ZonedDateTime
        //val redeemDate = dateTimeStringToZonedDateTime(properties["redeemDate"] as String)
        val redeemDate = ZonedDateTime.parse(properties["redeemDate"] as String, DateTimeFormatter.ISO_OFFSET_DATE_TIME)


        val itemId = properties["itemId"] as Int
        val itemName = properties["itemName"] as String
        val itemValue = properties["itemValue"] as Double
        val residentId = properties["residentId"] as String
        return PointsRedeemHistory(
            redeemDate,
            itemId,
            itemName,
            itemValue,
            residentId


        )
    }

}
