package healthcare.alliedworld.dispergo.redeempoint.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView

import healthcare.alliedworld.dispergo.redeempoint.entity.PointsRedeemHistory
import org.threeten.bp.ZoneOffset
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter
import reach52.marketplace.community.R
import reach52.marketplace.community.databinding.RedeemitemBinding

class RedeempointAdapter(
    val context: Activity,
    private var PointsRedeemList: List<PointsRedeemHistory>,
    private val onItemClick: (item: PointsRedeemHistory) -> Unit = {}
) : RecyclerView.Adapter<RedeempointAdapter.RedeemPointViewHolder>() {

    inner class RedeemPointViewHolder(val binding: RedeemitemBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)=
        RedeemPointViewHolder(
            DataBindingUtil.inflate<RedeemitemBinding>(
            LayoutInflater.from(parent.context), R.layout.redeemitem, parent, false))

    override fun onBindViewHolder(holder: RedeemPointViewHolder, position: Int) {
        val item = PointsRedeemList[position]
        holder.binding.rocstarcode1.text= item.itemName
        holder.binding.status2.text= item.itemValue.toString()+" pts redeemed"
        holder.binding.dateTextView.text=  item.redeemDate.format(DateTimeFormatter.ofPattern("MMM dd, yyyy"))

        Log.d("item value", "onBindViewHolder: "+item.itemValue.toString())
        if(item.itemValue.toString()=="0" || item.itemValue.toString()=="0.0")
        {
            holder.binding.indicator.text="+"
            holder.binding.indicator.setTextColor(ContextCompat.getColor(context, R.color.radiob))
            holder.binding.status2.visibility= View.INVISIBLE
        }




    }

    override fun getItemCount(): Int = PointsRedeemList.size


}