package healthcare.alliedworld.dispergo.redeempoint.repo

import android.content.Context
import android.util.Log
import healthcare.alliedworld.dispergo.redeempoint.entity.PointsRedeemHistory
import healthcare.alliedworld.dispergo.redeempoint.mapper.PointsRedeemHistoryeMapper

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter
import reach52.marketplace.community.persistence.DispergoDatabase
import java.util.*
import kotlin.collections.ArrayList

object PointsRedeemHistoryeRepo {

    //val mapper = PointsRedeemHistoryeMapper()

    fun getAllPointsRedeemHistory(context: Context,  id: String) = Single.create<ArrayList<PointsRedeemHistory>>{

        //val doc = DispergoDatabase.getInstance(context).getExistingDocument(id)
        val view = DispergoDatabase.redeempointView(context)
        val query = view.createQuery()
        query.startKey =id
        query.endKey = id
        query.runAsync{ rows, _ ->

            val mapper = PointsRedeemHistoryeMapper()
            val pointsRedeemHistory = ArrayList<PointsRedeemHistory>()
            Log.d("rowscount", "getAllPointsRedeemHistory: "+rows.count)

            rows.forEach {
                Log.d("dataaaa", "getAllPointsRedeemHistory: "+it.toString())
                try {

                    pointsRedeemHistory += mapper.unmarshal(it.document.properties)

                  //  val pointsRedeemHistory = GsonMapperUtil().getRedeemPointEntity(it.document.properties)
                  //  pointsRedeemHistory.redeemDate = ZonedDateTime.parse(pointsRedeemHistory.redeemDate.toString(), DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                   // pointsRedeemHistory += pointsRedeemHistory
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
          /*  pointsRedeemHistory.sortWith { o1, o2 ->
                o2.redeemDate.compareTo(o1.redeemDate)
            }*/
            it.onSuccess(pointsRedeemHistory)
        }
    }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    fun savepointRedeemPoint(context: Context, item: PointsRedeemHistory) = Completable.create {
        val mapper = PointsRedeemHistoryeMapper()
        val database = DispergoDatabase.getInstance(context)
        database.createDocument().run {

            putProperties(mapper.marshal(item))
            Log.d("taaaggg", "redeem doc id: ${id}")

            id

        }

        it.onComplete()

    }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())


}