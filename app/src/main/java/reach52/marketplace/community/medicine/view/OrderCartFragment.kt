package reach52.marketplace.community.medicine.view

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import healthcare.alliedworld.dispergo.redeempoint.entity.PointsRedeemHistory
import reach52.marketplace.community.R
import reach52.marketplace.community.activities.BaseActivity
import reach52.marketplace.community.adapters.SwipeHelper
import reach52.marketplace.community.databinding.LayoutPurchaseCompleteDialogBinding
import reach52.marketplace.community.databinding.OrderCartFragmentBinding
import reach52.marketplace.community.extensions.utils.getCurrencyString
import reach52.marketplace.community.medicine.adapter.SelectedMedicinesAdapter
import reach52.marketplace.community.medicine.viewmodel.MedicationPurchaseViewModel
import reach52.marketplace.community.medicine.viewmodel.MedicineViewModel
import io.reactivex.disposables.CompositeDisposable
import org.threeten.bp.ZoneOffset
import org.threeten.bp.ZonedDateTime
import reach52.marketplace.community.BuildConfig


@ExperimentalUnsignedTypes
class OrderCartFragment : Fragment() {
	private lateinit var vmed: MedicineViewModel
	private val disposables = CompositeDisposable()
	private lateinit var vm: MedicationPurchaseViewModel
	private lateinit var b: OrderCartFragmentBinding

	lateinit var adapter: SelectedMedicinesAdapter
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
							  savedInstanceState: Bundle?): View? {
		//(activity as MedicationPurchaseActivity).supportActionBar?.title = getString(R.string.medications)

		val activity = (activity as MedicationPurchaseActivity)
		vm = ViewModelProvider(activity)[MedicationPurchaseViewModel::class.java]
		vmed = ViewModelProvider(activity as BaseActivity)[MedicineViewModel::class.java]


		if (vm.selectedMedicines.value!!.isNotEmpty()) {
			vm.selectedMedicines.value!!.clear()
		}

		b = OrderCartFragmentBinding.inflate(inflater, container, false)
		b.vm = vm
		b.residentAgeTextView.text = vm.resident.getAgeString()

		b.addMedicationButton.setOnClickListener {
			activity.goToMedicineListFragment()
		}

		if (BuildConfig.BUILD_TYPE == "debug") {
			b.addMedicationButton.setOnLongClickListener {


				vmed.medicines.value?.subList(0, 20)?.forEach {
					vm.addMedicine(it, it.brandName.split(" ").first().length,
						it.brandName.split(" ").first().length)
				}

				true
			}
		}

		val savingPurchaseDialog = AlertDialog.Builder(context!!)
			.setMessage(getString(R.string.saving))
			.setCancelable(false)
			.create()

		val confirmationDialog = AlertDialog.Builder(context!!)
			.setMessage(getString(R.string.confirm_this_purchase))
			.setPositiveButton(R.string.yes) { dialog, _ ->

				b.checkoutButton.isEnabled = false
				savingPurchaseDialog.show()
				vm.saveOrder(context!!).subscribe({
					val rt= vm.total_points_value+ vm.mainPointTotal
					Log.d("main total", "saveOrder: "+vm.total_points_value+"  "+ vm.mainPointTotal)
					vm.updateredeempointDb(requireContext(), vm.resident.id,rt, vm.resident).subscribe{
						Log.d("update res", "updateredeempointDb: "+ vm.updatedata)

						val item = PointsRedeemHistory(
							ZonedDateTime.now(ZoneOffset.UTC),
							0,
							"" + vm.total_points_value.toString() + " pts earned",
							0.0,
							vm.resident.id
						)

						vm.savepointRedeemPoint(requireContext(), item).subscribe {


							dialog.dismiss()
							savingPurchaseDialog.dismiss()
							showOrderPlaceDialog()
						}
					}

				}, {
					b.checkoutButton.isEnabled = true
					savingPurchaseDialog.dismiss()
					Toast.makeText(context!!, it.message, Toast.LENGTH_SHORT).show()
				})
			}
			.setNegativeButton(R.string.no) { dialog, _ ->
				dialog.dismiss()
			}
			.create()

		b.checkoutButton.setOnClickListener {

			val req = vm.checkForPrescriptionRequired()
			if (req) {

				try {
					vm.validatePrescription()
					confirmationDialog.show()
				} catch (e: Exception) {
					when (e) {
						is MedicationPurchaseViewModel.PrescriptionNumberMissingException,
						is MedicationPurchaseViewModel.PrescriptionImageMissingException -> {
							activity.goToPrescriptionFragment()
						}
					}
				}

			} else {
				confirmationDialog.show()
			}

		}
		b.checkoutButton.isEnabled = false

		b.selectedMedicationsRecyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
		val itemTouchHelper = ItemTouchHelper(object : SwipeHelper(b.selectedMedicationsRecyclerView) {
			override fun instantiateUnderlayButton(position: Int): List<UnderlayButton> {
				var buttons = listOf<UnderlayButton>()
				val deleteButton = deleteButton(position)
				buttons = listOf(deleteButton)
				/*	//val markAsUnreadButton = markAsUnreadButton(position)
                    //	val archiveButton = archiveButton(position)
                    when (position) {
                        //1 -> buttons = listOf(deleteButton)
                        //2 -> buttons = listOf(deleteButton, markAsUnreadButton)
                        //3 -> buttons = listOf(deleteButton, markAsUnreadButton, archiveButton)
                        else -> Unit
                    }*/
				return buttons
			}
		})

		itemTouchHelper.attachToRecyclerView(b.selectedMedicationsRecyclerView)

		vm.selectedMedicines.observe(activity) {
			//val items = ArrayList(it.sortedWith(compareBy { it.medicine.supplierCode }))
			// Group All products by supplier
			adapter = SelectedMedicinesAdapter(context!!, it,
				{

					vm.calculatePricing()
					updatePricingUI()
					vm.selectedMedCount.postValue(it.size)
				}, {

					var emptyItems = 0
					vm.selectedMedicines.value?.forEach {
						if (it.qty == -1) {
							emptyItems++
						}
					}
					b.checkoutButton.isEnabled = emptyItems == 0

				})
			b.selectedMedicationsRecyclerView.adapter = adapter

		}
		return b.root
	}


	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setHasOptionsMenu(true)
	}


	private fun deleteButton(position: Int): SwipeHelper.UnderlayButton {
		return SwipeHelper.UnderlayButton(
			context!!,
			getString(R.string.delete),
			14.0f,
			android.R.color.holo_red_light,
			object : SwipeHelper.UnderlayButtonClickListener {
				override fun onClick() {
					//toast()
					Log.d("DeleteCheck", "onClick: " + position)
					adapter.removeMedication(position)
				}
			})
	}


	override fun onPrepareOptionsMenu(menu: Menu) {
		super.onPrepareOptionsMenu(menu)
		menu.findItem(R.id.action_cart).isVisible = true
		//MedicationPurchaseActivity.selecteMedsCount
		if (vm.selectedMedicines.value!!.size == 0) {
			vm.selectedMedicines.value!!.clear()
			menu.findItem(R.id.action_cart).isVisible = false

		}

	}

	override fun onResume() {
		super.onResume()
		(activity as MedicationPurchaseActivity).supportActionBar?.title = getString(R.string.medications)
	}


	fun updatePricingUI() {

		if (vm.discountNumber.isEmpty()) {
			b.discountIdNumberLabelTextView.visibility = View.GONE
			b.discountIdNumberTextView.visibility = View.GONE
		}

		b.subTotalTextView.text = getCurrencyString(vm.subTotal.toString(), vm.currency)
		b.feeTextView.text = getCurrencyString(vm.deliveryFee.toString(), vm.currency)
		b.taxTextView.text = getCurrencyString(vm.taxAmt.toString(), vm.currency)
		b.totalTextView.text = getCurrencyString(vm.total.toString(), vm.currency)
		b.subTotalTextView.text = getCurrencyString(vm.subTotal.toString(), vm.currency)
		b.feeTextView.text = getCurrencyString(vm.deliveryFee.toString(), vm.currency)
		b.taxTextView.text = getCurrencyString(vm.taxAmt.toString(), vm.currency)
		b.discountTextView.text = "- " + getCurrencyString("${vm.discountAmt}", vm.currency)
		b.totalTextView.text = getCurrencyString(vm.total.toString(), vm.currency)
		b.subTextViewPoint.text= vm.total_points_value.toString()

		b.checkoutButton.isEnabled = !vm.selectedMedicines.value!!.isEmpty()
	}


	fun showOrderPlaceDialog() {
		val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(activity!!)
		val binding: LayoutPurchaseCompleteDialogBinding =
			DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_purchase_complete_dialog, null, false)
		dialogBuilder.setView(binding.root)
		val alertDialog: AlertDialog = dialogBuilder.create()
		binding.tvPurchaseCompleted.text = getString(R.string.order_created)
		alertDialog.show()
		Handler().postDelayed({
			alertDialog.dismiss()
			activity!!.finish()
		}, 2000)

	}

}




